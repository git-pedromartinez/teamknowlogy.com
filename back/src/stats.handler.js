import * as mysql from "mysql";
const con = mysql.createConnection({
    host: "database-dna.cv8ipl70rvse.us-east-2.rds.amazonaws.com",
    user: "admin",
    port: "3306",
    password: "admin1234",
    database: "dna",
});
export const handler = async (event, context, callback) => {
    var sql = "call `dna`.`getStats`";
    return new Promise(function (resolve, reject) {
        con.query(sql, function (err, result, fields) {
            if (err) {
                return reject(err);
            }
            let { total, count_mutation, count_no_mutation } = result[0][0];
            let response = {
                statusCode: 200,
                body: {
                    count_mutation,
                    count_no_mutation,
                    ratio:
                        count_no_mutation > 0
                            ? count_mutation / count_no_mutation
                            : 0,
                },
            };
            resolve(response);
        });
    });
};
