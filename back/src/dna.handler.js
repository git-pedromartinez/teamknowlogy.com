import * as mysql from "mysql";
const con = mysql.createConnection({
    host: "database-dna.cv8ipl70rvse.us-east-2.rds.amazonaws.com",
    user: "admin",
    port: "3306",
    password: "admin1234",
    database: "dna",
});
class DNA {
    constructor() {
        this.ATCG = "ATCG";
        this.LENGTH_MUTATION = 4;
        this.NITROGEN_BASE = new RegExp("[" + this.ATCG + "]", "g");
    }
    generateMatriz(arr, dna) {
        let lenghMatriz = dna.length;
        for (let i = 0; i < lenghMatriz; i++) {
            let dnaSimple = dna[i].split("");
            if (dnaSimple.length != lenghMatriz || lenghMatriz < 4) {
                return false;
            }
            if (!this.valideDNA(dnaSimple)) {
                return false;
            }
            arr[i] = dnaSimple;
        }
        return true;
    }
    valideDNA(dnaSimple) {
        let largo = dnaSimple.length;
        for (let i = 0; i < largo; i++) {
            if (
                dnaSimple[i] != "A" &&
                dnaSimple[i] != "T" &&
                dnaSimple[i] != "C" &&
                dnaSimple[i] != "G"
            ) {
                return false;
            }
        }
        return true;
    }
    findSecuenceHoriz(arr, lenghMatriz) {
        let findSecuenceHoriz = 0;
        for (let i = 0; i < lenghMatriz; i++) {
            for (let j = 0; j < lenghMatriz; j++) {
                if (lenghMatriz - j >= 4) {
                    if (
                        arr[i][j] == arr[i][j + 1] &&
                        arr[i][j] == arr[i][j + 2] &&
                        arr[i][j] == arr[i][j + 3]
                    ) {
                        findSecuenceHoriz++;
                        j = j + 3;
                        if (findSecuenceHoriz > 1) {
                            return findSecuenceHoriz;
                        }
                    }
                }
            }
        }
        return findSecuenceHoriz;
    }
    findSecuenceVert(arr, lenghMatriz) {
        let findSecuenceVert = 0;
        for (let i = 0; i < lenghMatriz; i++) {
            for (let j = 0; j < lenghMatriz; j++) {
                if (lenghMatriz - i >= 4) {
                    if (
                        arr[i][j] == arr[i + 1][j] &&
                        arr[i][j] == arr[i + 2][j] &&
                        arr[i][j] == arr[i + 3][j]
                    ) {
                        findSecuenceVert++;
                        j = j + 3;
                        if (findSecuenceVert > 1) {
                            return findSecuenceVert;
                        }
                    }
                }
            }
        }
        return findSecuenceVert;
    }
    searchDiagIzqDer(arr, lenghMatriz, i, j, indLimit) {
        let contSecuencia = 0;
        let limit = 0;
        while (i <= lenghMatriz - 1 && j <= lenghMatriz - 1) {
            if (indLimit == "J") {
                limit = lenghMatriz - j;
            } else {
                limit = lenghMatriz - i;
            }
            if (limit >= 4) {
                if (
                    arr[i][j] == arr[i + 1][j + 1] &&
                    arr[i][j] == arr[i + 2][j + 2] &&
                    arr[i][j] == arr[i + 3][j + 3]
                ) {
                    contSecuencia++;
                }
            }
            i++;
            j++;
        }
        return contSecuencia;
    }
    findSecuenceDiagIzqDer(arr, lenghMatriz) {
        let i = 0;
        let j = 0;
        let countSecuence = 0;
        for (let indI = lenghMatriz - 1; indI >= 0; indI--) {
            if (indI == 0) {
                for (let indJ = 0; indJ < lenghMatriz; indJ++) {
                    i = indI;
                    j = indJ;
                    let cont = this.searchDiagIzqDer(
                        arr,
                        lenghMatriz,
                        i,
                        j,
                        "J"
                    );
                    countSecuence = countSecuence + cont;
                }
            } else {
                i = indI;
                let cont = this.searchDiagIzqDer(arr, lenghMatriz, i, j, "I");
                countSecuence = countSecuence + cont;
            }
            if (countSecuence > 1) {
                return countSecuence;
            }
        }
        return countSecuence;
    }
    searchDiagDerIzq(arr, lenghMatriz, i, j, indLimit) {
        let contSecuencia = 0;
        let limit = 0;
        while (i <= lenghMatriz - 1 && j >= 0) {
            if (indLimit == "J") {
                limit = j;
            } else {
                limit = lenghMatriz - i - 1;
            }
            if (limit >= 3) {
                if (
                    arr[i][j] == arr[i + 1][j - 1] &&
                    arr[i][j] == arr[i + 2][j - 2] &&
                    arr[i][j] == arr[i + 3][j - 3]
                ) {
                    contSecuencia++;
                }
            }
            i++;
            j--;
        }
        return contSecuencia;
    }
    findSecuenceDiagDerIzq(arr, lenghMatriz) {
        let i = 0;
        let j = 0;
        let countSecuence = 0;
        for (let indJ = 0; indJ <= lenghMatriz - 1; indJ++) {
            if (indJ == lenghMatriz - 1) {
                for (let indI = 0; indI <= lenghMatriz - 1; indI++) {
                    i = indI;
                    j = indJ;
                    let cont = this.searchDiagDerIzq(
                        arr,
                        lenghMatriz,
                        i,
                        j,
                        "I"
                    );
                    countSecuence = countSecuence + cont;
                }
            } else {
                j = indJ;
                let cont = this.searchDiagDerIzq(arr, lenghMatriz, i, j, "J");
                countSecuence = countSecuence + cont;
            }
            if (countSecuence > 1) {
                return countSecuence;
            }
        }
        return countSecuence;
    }
    hasMutation(dna) {
        let lenghMatriz = dna.length;
        let arr = new Array(lenghMatriz).map((e) => new Array(lenghMatriz));
        if (!this.generateMatriz(arr, dna)) {
            return false;
        }
        let totalSecuenceHoriz = 0;
        totalSecuenceHoriz = this.findSecuenceHoriz(arr, lenghMatriz);
        if (totalSecuenceHoriz > 1) {
            return true;
        }
        let totSecuenceVert = 0;
        totSecuenceVert = this.findSecuenceVert(arr, lenghMatriz);
        if (totalSecuenceHoriz + totSecuenceVert > 1) {
            return true;
        }
        let totalSecuenceDiagIzqDer = 0;
        totalSecuenceDiagIzqDer = this.findSecuenceDiagIzqDer(arr, lenghMatriz);
        if (
            totalSecuenceHoriz + totSecuenceVert + totalSecuenceDiagIzqDer >
            1
        ) {
            return true;
        }
        let totalSecuenceDiagDerIzq = 0;
        totalSecuenceDiagDerIzq = this.findSecuenceDiagDerIzq(arr, lenghMatriz);
        if (
            totalSecuenceHoriz +
                totSecuenceVert +
                totalSecuenceDiagIzqDer +
                totalSecuenceDiagDerIzq >
            1
        ) {
            return true;
        }
        return false;
    }
}
export const handler = async (event) => {
    let hasMutation = false;
    let response = null;
    const dna =
        typeof event.body === "string"
            ? JSON.parse(event.body)?.dna
            : event.body?.dna;
    try {
        hasMutation = new DNA().hasMutation(dna);
        if (hasMutation) {
            response = {
                statusCode: 200,
                body: hasMutation,
            };
        } else {
            response = {
                statusCode: 409,
                body: hasMutation,
            };
        }
    } catch (err) {
        response = {
            statusCode: 500,
            body: JSON.stringify({ message: "Error in hasMutation", dna }),
        };
    }

    //insert in database
    const sql = `INSERT INTO dna.hasMutationResponse (value,hasMutation) VALUES ('${dna}',${
        response.statusCode == 500 ? -1 : hasMutation ? 1 : 0
    })`;
    return new Promise(function (resolve, reject) {
        con.query(sql, function (err, result, fields) {
            if (err) {
                return reject(response);
            }
            //console.log("response", response);
            resolve(response);
        });
    });
};
