import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Chart, registerables } from 'chart.js';
Chart.register(...registerables);
import * as Utils from './src/utils';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
})
export class ChartComponent implements OnInit, AfterViewInit {
  chart: any;
  constructor() {}
  ngOnInit(): void {
    this.renderChart();
  }
  ngAfterViewInit(): void {}

  renderChart() {
    let itemsX = new Array(6).fill('Day ').map((e, index) => e + (index + 1));
    const RADIUS: number = 10;
    let numbersA: any[] = Utils.numbers({
      count: itemsX.length,
      min: -100,
      max: 100,
    });
    let numbersB: any[] = Utils.numbers({
      count: itemsX.length,
      min: -100,
      max: 100,
    });

    let numbersC: any[] = numbersB.map((e, index) =>
      index == numbersB.length / 2 ? e : null
    );

    this.chart = new Chart('mychart', {
      type: 'line',
      data: {
        labels: itemsX,
        datasets: [
          {
            label: 'Dataset',
            data: numbersA,
            borderColor: Utils.CHART_COLORS.blue,
            backgroundColor: Utils.transparentize(Utils.CHART_COLORS.blue, 0.5),
            pointStyle: () => this.renderCircle2(RADIUS),
            pointRadius: 10,
            pointHoverRadius: 15,
            tension: 0.3,
          },
          {
            label: 'Dataset 2',
            data: numbersB,
            borderColor: Utils.CHART_COLORS.yellow,
            backgroundColor: Utils.transparentize(
              Utils.CHART_COLORS.yellow,
              0.7
            ),
            pointStyle: 'circle',
            pointRadius: 10,
            pointHoverRadius: 15,
            tension: 0.3,
          },
          {
            label: 'Dataset 2',
            data: numbersC,
            borderColor: Utils.CHART_COLORS.yellow,
            backgroundColor: Utils.transparentize(
              Utils.CHART_COLORS.yellow,
              0.1
            ),
            pointStyle: 'circle',
            pointRadius: 5,
            pointHoverRadius: 10,
            tension: 0.3,
          },
        ],
      },
      options: {
        responsive: true,
        plugins: {
          title: {
            display: true,
            //@ts-ignore
            text: (ctx) =>
              'Point Style: ' + ctx.chart.data.datasets[0].pointStyle,
          },
          tooltip: {},
        },
      },
    });
  }

  renderIMG() {
    let img = new Image(24, 24);
    img.src =
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTeeQZLFDimeACGPe2lyYlD5rJ1eaiYoUaR8g&usqp=CAU';
    return img;
  }

  renderCircle(radius: number) {
    var canvas = <HTMLCanvasElement>document.getElementById('mycircle');
    if (canvas && canvas.getContext) {
      var ctx = canvas.getContext('2d');
      if (ctx) {
        var X = canvas.width / 2;
        var Y = canvas.height / 2;
        var r = radius;
        ctx.strokeStyle = Utils.CHART_COLORS.blue;
        ctx.fillStyle = Utils.transparentize(Utils.CHART_COLORS.blue, 0.5);
        ctx.lineWidth = radius / 10;
        ctx.arc(X, Y, r, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();
      }
    }
    return canvas;
  }

  renderCircle2(radius: number) {
    let canvas = <HTMLCanvasElement>(
      document.getElementById('mycircle').cloneNode(true)
    );
    let ctx = canvas.getContext('2d');

    let circleColorA = 'blue';
    let circleColorB = 'red';

    let cx = canvas.width / 2;
    let cy = canvas.height / 2;

    canvas.onclick = function (event) {
      var x = event.clientX;
      var y = event.clientY;

      var dx = Math.abs(x - cx);
      var dy = Math.abs(y - cy);

      var distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));

      if (distance <= radius) {
        ctx.beginPath();
        ctx.arc(cx, cy, radius, 0, 2 * Math.PI);
        ctx.fillStyle = circleColorA;
        ctx.fill();
      } else {
        ctx.beginPath();
        ctx.arc(cx, cy, radius, 0, 2 * Math.PI);
        ctx.fillStyle = circleColorB;
        ctx.fill();
      }
    };
    ctx.beginPath();
    ctx.arc(cx, cy, radius, 0, 2 * Math.PI);
    ctx.fillStyle = circleColorA;
    ctx.fill();
    return canvas;
  }
}
